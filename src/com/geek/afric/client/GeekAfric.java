package com.geek.afric.client;

import com.geek.afric.shared.User;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * @author Cheick Mahady SISSOKO
 */
public class GeekAfric implements EntryPoint {

	private User user;
	private final GeekServiceAsync rpcService = GWT
			.create(GeekService.class);
	private final HandlerManager eventBus = new HandlerManager(null);
	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		com.google.gwt.dom.client.Element loading = DOM
				.getElementById("loading");
		RootPanel.getBodyElement().removeChild(loading);
		String sessionID = Cookies.getCookie("sid");
		if (sessionID == null) {
			user = User.guest();
			AppManager appViewer = new AppManager(eventBus, rpcService,
					RootPanel.get("top"), RootPanel.get("left"), user);
			appViewer.go(RootPanel.get("content"));
		} else {
			rpcService.getUserFromSession(sessionID, new AsyncCallback<User>() {

				@Override
				public void onSuccess(User result) {
					user = result;
					if (user == null) {
						user = User.guest();
					}
					AppManager appViewer = new AppManager(eventBus,
							rpcService, RootPanel.get("top"), RootPanel
									.get("left"), user);

					appViewer.go(RootPanel.get("content"));
				}

				@Override
				public void onFailure(Throwable caught) {
					user = User.guest();
					AppManager appViewer = new AppManager(eventBus,
							rpcService, RootPanel.get("top"), RootPanel
									.get("left"), user);
					appViewer.go(RootPanel.get("content"));
				}
			});
		}
	}
}
